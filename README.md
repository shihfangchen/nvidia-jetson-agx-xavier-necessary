### pwm-fan

> sudo sh -c 'echo 150 > /sys/devices/pwm-fan/target_pwm'

-> [guide](https://blog.cavedu.com/2019/10/04/nvidia-jetson-nano-fan/)

### rootOnNVMe

> Formate DISK -> Create Partition -> mount_Partition

> git clone https://github.com/jetsonhacks/rootOnNVMe.git

> cd rootOnNVMe

> ./copy-rootfs-ssd.sh

> ./setup-service.sh

> init 6

### swap

> df -h

This will show your file system and how much space you have left.

> sudo fallocate -l 8.0G /swapfile

Create a 8 GB of swap

> sudo chmod 600 /swapfile

Change file permissions

> sudo mkswap /swapfile

> sudo swapon /swapfile

> free -m

This will show you the swap file is on. You can also pull up the System Monitor
However this is only temporary. If you reboot, swap file is gone.

> sudo vim /etc/fstab

Within this file add the line “/swapfile none swap 0 0”. 
Do not include the quotes.
Exit and save file

> init 6

Now you can reboot and your Swap will be activated.


-> [guide](https://forums.developer.nvidia.com/t/creating-a-swap-file/65385)

### buildOpenCVXavier

> git clone https://github.com/jetsonhacks/buildOpenCVXavier.git

> cd buildOpenCVXavier

> ./buildOpenCV.sh

### Pytorch

-> [guide](https://forums.developer.nvidia.com/t/
pytorch-for-jetson-nano-version-1-5-0-now-available/72048/390)

-> [torchvision](https://gist.github.com/heavyinfo/8d0ef5a3768c5d8d22e164863670330a)

### SSH

> sudo apt-get install -y openssh-server

>  sudo service ssh status

-> [guide](https://oranwind.org/post-post-10/)


send file
> rsync /path/to/local/file username@PCB:/path/to/remote/destination

### librealsense on jetson

-> [guide for C++](https://github.com/IntelRealSense/librealsense/blob/master/doc/installation_jetson.md)


-> then install [Python](https://github.com/IntelRealSense/librealsense/tree/master/wrappers/python#building-from-source)

build Python on the root of librealsense repo

> mkdir build && cd build

> cmake ../ -DBUILD_PYTHON_BINDINGS:bool=true -DPYTHON_EXECUTABLE="/usr/bin/python3.6" -DFORCE_LIBUVC=true

Note that set -DFORCE_LIBUVC=true to activate depth camera

### set CMakeLists.txt to fix OPENGL problem(passed test on UBUNTU 1804)

add following script to CMakeLists.txt

```
if (POLICY CMP0072)
  set(OpenGL_GL_PREFERENCE LEGACY)
endif()
```

Make sure NVIDIA DRIVER install correctly, it help you install correct OPENGL version

In build/

> cmake ../ -DCMAKE_BUILD_TYPE=Release -DBUILD_PYTHON_BINDINGS:bool=true -DBUILD_EXAMPLES=true -DBUILD_GRAPHICAL_EXAMPLES=true -DBUILD_CV_EXAMPLES=true -DPYTHON_EXECUTABLE="/usr/bin/python3.6" -DFORCE_LIBUVC=true

> make -j8

> sudo make install

> export PYTHONPATH=$PYTHONPATH:/usr/local/lib
