import numpy as np
import cv2
import os

# get_number_of_camera_devices
number_of_camera_devices  = os.popen('ls -ltrh /dev/video* | wc -l').readlines()
number_of_camera_devices = int(number_of_camera_devices[0])
print ('number_of_camera_devices' , number_of_camera_devices)

# get_BGR_CAMERA_INDEX
bgr_camera_index = -1
detect_gray_count = 0
for i in range(0, number_of_camera_devices):

    if detect_gray_count == 2:
        i = 0

    cap = cv2.VideoCapture(i)
    if cap.isOpened():
        print("load  ",i)
        ret, frame = cap.read() 
        if (cv2.mean(frame)[1]!=0):
            if cv2.mean(frame)[1] != 0:
                bgr_camera_index = i
                print("open ",i)
            else:
                detect_gray_count = detect_gray_count + 1

cap.release()
# load_VideoCapture
print("\nbgr_camera_index ",bgr_camera_index,"\n")
cap = cv2.VideoCapture(bgr_camera_index)
print("cap.isOpened() ",cap.isOpened())

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
